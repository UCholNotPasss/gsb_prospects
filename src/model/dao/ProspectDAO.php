<?php
/**
 * File :        ProspectDAO.php
 * Location :    gsb_prospects/src/model/dao/ProspectDAO.php
 * PHP Version : 7.0
 * 
 * @author  David RIEHL <david.riehl@ac-lille.fr>
 * @license GPL 3.0
 */
namespace gsb_prospects\model\dao;

use \PDO;
use \ReflectionClass;
use gsb_prospects\kernel\NotImplementedException;
use gsb_prospects\model\objects\Prospect;

/**
 * Class ProspectDAO
 * 
 * @author  David RIEHL <david.riehl@ac-lille.fr>
 * @license GPL 3.0
 */
final class ProspectDAO extends AbstractDAO implements IDAO
{
    protected $table = "prospect";
    protected $joinedTables = [
        [ "Type"=>"Inner", "Table"=>"praticien", "Foreign Table"=>"prospect", "Foreign Key"=>["id_praticien"], "Primary Table"=>"praticien", "Primary Key"=>["id"] ],
    ];
    protected $class = "gsb_prospects\model\objects\Prospect";
    protected $fields = [
        "id_Praticien", "id_Etat", "id", "nom", "prenom", "adresse", "id_Ville", "id_Type_Praticien", "fixe", "mobile", "fax", "email" 
    ];
    
    public function delete(&$object)
    {
        throw new NotImplementedException();
    }

    public function findAll()
    {
        $dbh = $this->getConnexion();

        $query  = "SELECT * FROM `{$this->table}`" . PHP_EOL;
        if (! empty($this->joinedTables)) {
            $query .= $this->join();
        }
        
        $sth = $dbh->prepare($query);
        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $sth->execute();

        $array = $sth->fetchAll();

        $this->closeConnexion();

        if ($array === false) {
            $message = $sth->errorInfo()[2];    // Error Message
            $code = $sth->errorInfo()[0];       // SQLSTATE
            if ($code != 0) {
                throw new DAOException($message, $code);
            }
        } else {
            $objects = [];
            foreach ($array as $row) {
                $reflectedClass = new ReflectionClass($this->class);
                $object = $reflectedClass->newInstanceArgs($row);
                $objects[] = $object;
            }
        }

        return $objects;
    }

    /**
     * AbstractDAO method Overriding
     * */
    public function insert(&$object)
    {
        $id = $object->getId();
        $query  = "INSERT INTO `{$this->table}`" . PHP_EOL;
        $query .= "(";
        $query .= "id_Praticien, id_Etat";
        $query .= ")" . PHP_EOL;
        $query .= "VALUES" . PHP_EOL;
        $query .= "(";
        $query .= ":id";
        $query .= ", ";
        $query .= "1";
        $query .= ");";
        
        $dbh = $this->getConnexion();
        $sth = $dbh->prepare($query);
        $sth->bindParam(":id", $id);

        $result = $sth->execute();
        $this->closeConnexion();
        
        if (!$result) {
            $message = $sth->errorInfo()[2];    // Error Message
            $code = $sth->errorInfo()[0];       // SQLSTATE
            if ($code != 0) {
                throw new DAOException($message, $code);
            }
        }
        return $result;
    }

     /**
     * AbstractDAO method Overriding
     */
    public function update($object)
    {
        $id = $object->getIdPraticien();
        $id2 = $object->getIdEtat();
        $query  = "UPDATE `{$this->table}`" . PHP_EOL;
        $query .= "SET" . PHP_EOL;
        $query .= "id_Etat =";
        $query .= ":id2" . PHP_EOL;
        $query .= "WHERE id_Praticien=";
        $query .= ":id1;";
        
        $dbh = $this->getConnexion();
        $sth = $dbh->prepare($query);
        $sth->bindParam(":id1", $id);
        $sth->bindParam(":id2", $id2);

        $result = $sth->execute();
        $this->closeConnexion();
        
        if (!$result) {
            $message = $sth->errorInfo()[2];    // Error Message
            $code = $sth->errorInfo()[0];       // SQLSTATE
            if ($code != 0) {
                throw new DAOException($message, $code);
            }
        }
        return $result;
    }
}