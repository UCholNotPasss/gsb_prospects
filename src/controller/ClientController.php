<?php
/**
 * File :        ClientController.php
 * Location :    gsb_prospects/src/controller/ClientController.php
 * PHP Version : 7.0
 * 
 * @author  David RIEHL <david.riehl@ac-lille.fr>
 * @license GPL 3.0
 */
namespace gsb_prospects\controller;

use gsb_prospects\kernel\Route;
use gsb_prospects\kernel\Router;
use gsb_prospects\model\dao\ClientDAO;
use gsb_prospects\model\dao\ProspectDAO;
use gsb_prospects\model\objects\Praticien;
use gsb_prospects\model\dao\PraticienDAO;
use gsb_prospects\model\dao\VilleDAO;
use gsb_prospects\model\dao\TypePraticienDAO;
use gsb_prospects\model\objects\Client;
use gsb_prospects\view\View;

/**
 * Class ClientController
 * 
 * @author  David RIEHL <david.riehl@ac-lille.fr>
 * @license GPL 3.0
 */
final class ClientController extends AbstractController implements IController
{
    /**
     * __construct
     */
    public function __construct()
    {
        $this->_dao = new ClientDAO();
        $this->_router = new Router();
        // 2nd level route definition
        $this->_router->addRoute(new Route("/clients", "ClientController", "listAction", "client_list"));
        $this->_router->addRoute(new Route("/client/create/{id}", "ClientController", "createAction", "client_create"));
    }

    /**
     * Procedure defaultAction
     *
     * @return void
     */
    public function defaultAction()
    {
        $route = $this->_router->findRoute();
        if ($route) {
            $route->execute();
        } else {
            print("<p>Page inconnue.</p>" . PHP_EOL);
        }
    }
 
    /**
     * Procedure listAction
     *
     * @return void
     */
    public function listAction()
    {
        $view = new View("Client_List");
        
        $view->bind("title", "Liste des Clients");
        $view->bind("objectName", "client");
        $view->bind("objectNamePlural", "clients");

        $basePath = $this->_router->getBasePath();
        $view->bind("basePath", $basePath);

        $objects = $this->_dao->findAll();
        $view->bind("objects", $objects);

        $view->display();
    }
    
    /**
     * Procedure createAction
     *
     * @return void
     */
    public function createAction($id)
    {
        if(!isset($_POST['transform']))
        {
            /**
             * Creation of View
             */
            $view = new View("Client_Create");
            $view->bind("title", "Création d'un client");
            $view->bind("objectName", "client");
            $view->bind("objectNamePlural", "clients");
    
            $basePath = $this->_router->getBasePath();
            $view->bind("basePath", $basePath);
    
            $objects = $this->_dao->findAll();
            $view->bind("objects", $objects);
            
            $this->_dao = new PraticienDAO();
            $praticiens = $this->_dao->find($id);
            $view->bind("pratics",$praticiens);
            
            $this->_dao = new VilleDAO();
            $city = $this->_dao->findFromPraticien($id);
            $view->bind("city", $city);
            
            $this->_dao = new TypePraticienDAO();
            $type = $this->_dao->findFromPraticien($id);
            $view->bind("type", $type);
    
            $view->display();
        }
        else
        {
            /**
             * Adding Prospect in database
             */
            $lname = filter_input(INPUT_POST, 'lname', FILTER_SANITIZE_STRING);
            $fname = filter_input(INPUT_POST, 'fname', FILTER_SANITIZE_STRING);
            $address = filter_input(INPUT_POST, 'address', FILTER_SANITIZE_STRING);
            $type = 1;
            $city = 1;
            $this->_dao = new PraticienDAO();
            $p = new Praticien($id,$lname,$fname,$address,$city,$type);
            $this->_dao = new ClientDAO();
            $prospects = $this->_dao->insert($p);
            $basePath = $this->_router->getBasePath();
            header("Location: ".$basePath."/clients/");
        }
    }
}