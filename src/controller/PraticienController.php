<?php
/**
 * File :        PraticienController.php
 * Location :    gsb_prospects/src/controller/PraticienController.php
 * PHP Version : 7.0
 * 
 * @author  David RIEHL <david.riehl@ac-lille.fr>
 * @license GPL 3.0
 */
namespace gsb_prospects\controller;

use gsb_prospects\kernel\Route;
use gsb_prospects\kernel\Router;
use gsb_prospects\model\dao\PraticienDAO;
use gsb_prospects\model\dao\VilleDAO;
use gsb_prospects\model\dao\AbstractDAO;
use gsb_prospects\model\dao\TypePraticienDAO;
use gsb_prospects\model\objects\Praticien;
use gsb_prospects\view\View;

ini_set('display_errors', 1);
ini_set('log_errors', 1);
ini_set('error_log', dirname(__FILE__) . '/error_log.txt');
error_reporting(E_ALL);

/**
 * Class PraticienController
 * 
 * @author  David RIEHL <david.riehl@ac-lille.fr>
 * @license GPL 3.0
 */
final class PraticienController extends AbstractController implements IController
{
    /**
     * __construct
     */
    public function __construct()
    {
        $this->_dao = new PraticienDAO();
        $this->_router = new Router();
        // 2nd level route definition
        $this->_router->addRoute(new Route("/praticiens", "PraticienController", "listAction", "praticien_list"));
        $this->_router->addRoute(new Route("/praticien/create","PraticienController","createAction","praticien_create"));
        $this->_router->addRoute(new Route("/praticien/update/{id}","PraticienController","updateAction","praticien_update"));
        $this->_router->addRoute(new Route("/praticien/delete/{id}","PraticienController","deleteAction","praticien_delete"));
    }

    /**
     * Procedure defaultAction
     *
     * @return void
     */
    public function defaultAction()
    {
        $route = $this->_router->findRoute();
        if ($route) {
            $route->execute();
        } else {
            print("<p>Page inconnue.</p>" . PHP_EOL);
        }
    }

    /**
     * Procedure listAction
     *
     * @return void
     */
    public function listAction()
    {
        $view = new View("Praticien_List");

        $view->bind("title", "Liste des Praticiens");
        $view->bind("objectName", "praticien");
        $view->bind("objectNamePlural", "praticiens");

        $basePath = $this->_router->getBasePath();
        $view->bind("basePath", $basePath);

        $objects = $this->_dao->findAll();
        $view->bind("objects", $objects);

        $view->display();
    }
    
    /**
     * Procedure createAction
     *
     * @return void
     */
    public function createAction()
    {
        if(!isset($_POST['add']))
        {
            /**
             * Creation of View
             * */
            $view = new View("Praticien_Create");
    
            $view->bind("title", "Ajouter un Praticien");
            $view->bind("objectName", "praticien");
            $view->bind("objectNamePlural", "praticiens");
    
            $basePath = $this->_router->getBasePath();
            $view->bind("basePath", $basePath);
    
            $this->_dao = new VilleDAO();
            $cities = $this->_dao->findAll();
            $view->bind("cities", $cities);
            
            $this->_dao = new TypePraticienDAO();
            $objects = $this->_dao->findAll();
            $view->bind("types", $objects);
    
            $view->display();
        }
        else
        {
            /**
             * Adding in database
             * */
            $lname = filter_input(INPUT_POST, 'lname', FILTER_SANITIZE_STRING);
            $fname = filter_input(INPUT_POST, 'fname', FILTER_SANITIZE_STRING);
            $address = filter_input(INPUT_POST, 'address', FILTER_SANITIZE_STRING);
            if($_POST['type'] != "0") $type = filter_input(INPUT_POST, 'type', FILTER_SANITIZE_NUMBER_INT); else $type = null;
            if($_POST['city'] != "0") $city = filter_input(INPUT_POST, 'city', FILTER_SANITIZE_NUMBER_INT); else $city = null;
            $this->_dao = new PraticienDAO();
            $p = new Praticien(null,$lname,$fname,$address,$city,$type,'','','','');
            $this->_dao->insert($p);
            $basePath = $this->_router->getBasePath();
            header("Location: ".$basePath."/praticiens/");
        }
    }
    
    /**
     * Procedure updateAction
     *
     * @return void
     */
    public function updateAction($id)
    {
        if(!isset($_POST['mod']))
        {
            /**
             * Creation of View
             * */
            $view = new View("Praticien_Update");
    
            $view->bind("title", "Modifier un Praticien");
            $view->bind("objectName", "praticien");
            $view->bind("objectNamePlural", "praticiens");
    
            $basePath = $this->_router->getBasePath();
            $view->bind("basePath", $basePath);
    
            $this->_dao = new PraticienDAO();
            $praticiens = $this->_dao->find($id);
            $view->bind("pratics",$praticiens);
            
            $this->_dao = new VilleDAO();
            $city = $this->_dao->findFromPraticien($id);
            $cities = $this->_dao->findAll();
            $view->bind("city", $city);
            $view->bind("cities",$cities);
            
            $this->_dao = new TypePraticienDAO();
            $type = $this->_dao->findFromPraticien($id);
            $types = $this->_dao->findAll();
            $view->bind("types", $types);
            $view->bind("type", $type);
    
            $view->display();
        }
        else
        {
            /**
             * Modifying in database
             * */
            $lname = filter_input(INPUT_POST, 'lname', FILTER_SANITIZE_STRING);
            $fname = filter_input(INPUT_POST, 'fname', FILTER_SANITIZE_STRING);
            $address = filter_input(INPUT_POST, 'address', FILTER_SANITIZE_STRING);
            if($_POST['type'] != "0") $type = filter_input(INPUT_POST, 'type', FILTER_SANITIZE_NUMBER_INT); else $type = null;
            if($_POST['city'] != "0") $city = filter_input(INPUT_POST, 'city', FILTER_SANITIZE_NUMBER_INT); else $city = null;
            $this->_dao = new PraticienDAO();
            $p = new Praticien($id,$lname,$fname,$address,$city,$type,'','','','');
            $this->_dao->update($p);
            $basePath = $this->_router->getBasePath();
            header("Location: ".$basePath."/praticiens/");
        }
    }
    
    /**
     * Procedure deleteAction
     *
     * @return void
     */
    public function deleteAction($id)
    {
        if(!isset($_POST['del']))
        {
            /**
             * Creation of View
             * */
            $view = new View("Praticien_Delete");
    
            $view->bind("title", "Supprimer un Praticien");
            $view->bind("objectName", "praticien");
            $view->bind("objectNamePlural", "praticiens");
    
            $basePath = $this->_router->getBasePath();
            $view->bind("basePath", $basePath);
    
            $this->_dao = new PraticienDAO();
            $praticiens = $this->_dao->find($id);
            $view->bind("pratics",$praticiens);
            
            $this->_dao = new VilleDAO();
            $city = $this->_dao->findFromPraticien($id);
            $view->bind("city", $city);
            
            $this->_dao = new TypePraticienDAO();
            $type = $this->_dao->findFromPraticien($id);
            $view->bind("type", $type);
    
            $view->display();
        }
        else
        {
            /**
             * Deleting from database
             * */
            $lname = filter_input(INPUT_POST, 'lname', FILTER_SANITIZE_STRING);
            $fname = filter_input(INPUT_POST, 'fname', FILTER_SANITIZE_STRING);
            $address = filter_input(INPUT_POST, 'address', FILTER_SANITIZE_STRING);
            $type = 1;
            $city = 1;
            $this->_dao = new PraticienDAO();
            $p = new Praticien($id,$lname,$fname,$address,$city,$type,'','','','');
            $this->_dao->delete($p);
            $basePath = $this->_router->getBasePath();
            header("Location: ".$basePath."/praticiens/");
        }
    }
}