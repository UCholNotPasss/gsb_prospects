<?php
/**
 * PHP version 7.0
 * gsb_prospects/src/model/dao/DAOException.php
 * 
 * @author  David RIEHL <david.riehl@ac-lille.fr>
 * @license GPL 3.0
 */
 namespace gsb_prospects\model\dao;

use gsb_prospects\kernel\AbstractException;

/**
 * Class DAOException
 * 
 * @author  David RIEHL <david.riehl@ac-lille.fr>
 * @license GPL 3.0
 */
 
/* La classe DAOException hérite de la classe AbstractException */
final class DAOException extends AbstractException
{
    
}
