<?php
/**
 * PHP version 7.0
 * gsb_prospects/src/model/dao/ClientDAO.php
 * 
 * @author  David RIEHL <david.riehl@ac-lille.fr>
 * @license GPL 3.0
 */
namespace gsb_prospects\model\dao;

use \PDO;
use \ReflectionClass;
use gsb_prospects\kernel\NotImplementedException;
use gsb_prospects\model\objects\Client;

/**
 * Class ClientDAO
 * 
 * @author  David RIEHL <david.riehl@ac-lille.fr>
 * @license GPL 3.0
 */
final class ClientDAO extends AbstractDAO implements IDAO
{
    /* Jointure de la table client à la table praticien */
    protected $table = "client";
    protected $joinedTables = [
        [ "Type"=>"Inner", "Table"=>"praticien", "Foreign Table"=>"client", "Foreign Key"=>["id_praticien"], "Primary Table"=>"praticien", "Primary Key"=>["id"] ],
    ];
    protected $class = "gsb_prospects\model\objects\Client";
    protected $fields = [
        "id_Praticien", "id", "nom", "prenom", "adresse", "id_Ville", "id_Type_Praticien"
    ];
    
    /* Préviens que la méthode n'est pas implémentée lors d'une suppression */
    public function delete(&$object)
    {
        throw new NotImplementedException();
    }

    /* SELECT récupérant toute les informations d'une table */
    public function findAll()
    {
        $dbh = $this->getConnexion();

        $query  = "SELECT * FROM `{$this->table}`" . PHP_EOL;
        if (! empty($this->joinedTables)) {
            $query .= $this->join();
        }
        
        $sth = $dbh->prepare($query);
        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $sth->execute();

        $array = $sth->fetchAll();

        $this->closeConnexion();

        if ($array === false) {
            $message = $sth->errorInfo()[2];    // Error Message
            $code = $sth->errorInfo()[0];       // SQLSTATE
            if ($code != 0) {
                throw new DAOException($message, $code);
            }
        } else {
            $objects = [];
            foreach ($array as $row) {
                $reflectedClass = new ReflectionClass($this->class);
                $object = $reflectedClass->newInstanceArgs($row);
                $objects[] = $object;
            }
        }

        return $objects;
    }

    /* Inserting pratician as client */
    public function insert(&$object)
    {
        $id = $object->getId();
        $query  = "INSERT INTO `{$this->table}`" . PHP_EOL;
        $query .= "(";
        $query .= "id_Praticien";
        $query .= ")" . PHP_EOL;
        $query .= "VALUES" . PHP_EOL;
        $query .= "(";
        $query .= ":id";
        $query .= ");";
        
        $dbh = $this->getConnexion();
        $sth = $dbh->prepare($query);
        $sth->bindParam(":id", $id);

        $result = $sth->execute();
        $this->closeConnexion();
        
        if (!$result) {
            $message = $sth->errorInfo()[2];    // Error Message
            $code = $sth->errorInfo()[0];       // SQLSTATE
            if ($code != 0) {
                throw new DAOException($message, $code);
            }
        }
        return $result;
    }

    /* Préviens que la méthode n'est pas implémenté lors d'une modification */
    public function update($object)
    {
        throw new NotImplementedException();
    }
}