<?php
/**
 * File :        ProspectController.php
 * Location :    gsb_prospects/src/controller/ProspectController.php
 * PHP Version : 7.0
 * 
 * @author  David RIEHL <david.riehl@ac-lille.fr>
 * @license GPL 3.0
 */
namespace gsb_prospects\controller;

use gsb_prospects\kernel\Route;
use gsb_prospects\kernel\Router;
use gsb_prospects\model\dao\ProspectDAO;
use gsb_prospects\model\objects\Praticien;
use gsb_prospects\model\objects\Etat;
use gsb_prospects\model\dao\PraticienDAO;
use gsb_prospects\model\dao\EtatDAO;
use gsb_prospects\model\dao\VilleDAO;
use gsb_prospects\model\dao\TypePraticienDAO;
use gsb_prospects\model\objects\Prospect;
use gsb_prospects\view\View;

/**
 * Class ProspectController
 * 
 * @author  David RIEHL <david.riehl@ac-lille.fr>
 * @license GPL 3.0
 */
final class ProspectController extends AbstractController implements IController
{
    /**
     * __construct
     */
    public function __construct()
    {
        $this->_dao = new ProspectDAO();
        $this->_router = new Router();
        // 2nd level route definition
        $this->_router->addRoute(new Route("/prospects", "ProspectController", "listAction", "prospect_list"));
        $this->_router->addRoute(new Route("/prospect/create/{id}", "ProspectController", "createAction", "prospect_create"));
        $this->_router->addRoute(new Route("/prospect/contact/{id}", "ProspectController", "contactAction", "prospect_contact"));
        $this->_router->addRoute(new Route("/prospect/update/{id}", "ProspectController", "updateAction", "prospect_update"));
    }

    /**
     * Procedure defaultAction
     *
     * @return void
     */
    public function defaultAction()
    {
        $route = $this->_router->findRoute();
        if ($route) {
            $route->execute();
        } else {
            print("<p>Page inconnue.</p>" . PHP_EOL);
        }
    }

    /**
     * Procedure listAction
     *
     * @return void
     */
    public function listAction()
    {
        $view = new View("Prospect_List");
        $view->bind("title", "Liste des Prospects");
        $view->bind("objectName", "prospect");
        $view->bind("objectNamePlural", "prospects");

        $basePath = $this->_router->getBasePath();
        $view->bind("basePath", $basePath);

        $objects = $this->_dao->findAll();
        $view->bind("objects", $objects);

        $view->display();
    }
    
    /**
     * Procedure createAction
     *
     * @return void
     */
    public function createAction($id)
    {
        if(!isset($_POST['prosp']))
        {
            /**
             * Creation of View
             */
            $view = new View("Prospect_Create");
            $view->bind("title", "Création des Prospects");
            $view->bind("objectName", "prospect");
            $view->bind("objectNamePlural", "prospects");
    
            $basePath = $this->_router->getBasePath();
            $view->bind("basePath", $basePath);
    
            $objects = $this->_dao->findAll();
            $view->bind("objects", $objects);
            
            $this->_dao = new PraticienDAO();
            $praticiens = $this->_dao->find($id);
            $view->bind("pratics",$praticiens);
            
            $this->_dao = new VilleDAO();
            $city = $this->_dao->findFromPraticien($id);
            $view->bind("city", $city);
            
            $this->_dao = new TypePraticienDAO();
            $type = $this->_dao->findFromPraticien($id);
            $view->bind("type", $type);
    
            $view->display();
        }
        else
        {
            /**
             * Adding Prospect in database
             */
            $lname = filter_input(INPUT_POST, 'lname', FILTER_SANITIZE_STRING);
            $fname = filter_input(INPUT_POST, 'fname', FILTER_SANITIZE_STRING);
            $address = filter_input(INPUT_POST, 'address', FILTER_SANITIZE_STRING);
            $type = 1;
            $city = 1;
            $this->_dao = new PraticienDAO();
            $p = new Praticien($id,$lname,$fname,$address,$city,$type);
            $this->_dao = new ProspectDAO();
            $prospects = $this->_dao->insert($p);
            $basePath = $this->_router->getBasePath();
            header("Location: ".$basePath."/prospects/");
        }
    }
    
    /**
     * Procedure contactAction
     *
     * @return void
     */
    public function contactAction($id)
    {
        $view = new View("Prospect_Contact");
        $view->bind("title", "Contacter un Prospects");
        $view->bind("objectName", "prospect");
        $view->bind("objectNamePlural", "prospects");

        $basePath = $this->_router->getBasePath();
        $view->bind("basePath", $basePath);

        $objects = $this->_dao->find($id);
        $view->bind("objects", $objects);
        
        $this->_dao = new VilleDAO();
        $city = $this->_dao->findFromPraticien($id);
        $view->bind("city", $city);
        
        $this->_dao = new TypePraticienDAO();
        $type = $this->_dao->findFromPraticien($id);
        $view->bind("type", $type);

        $view->display();
    }
    
    /**
     * Procedure updateAction
     *
     * @return void
     */
    public function updateAction($id)
    {
        if(!isset($_POST['mod']))
        {
            /**
             * Creation of View
             */
            $view = new View("Prospect_Update");
            $view->bind("title", "Modifier un Prospect");
            $view->bind("objectName", "prospect");
            $view->bind("objectNamePlural", "prospects");
    
            $basePath = $this->_router->getBasePath();
            $view->bind("basePath", $basePath);
    
            $objects = $this->_dao->findAll();
            $view->bind("objects", $objects);
            
            $this->_dao = new PraticienDAO();
            $praticiens = $this->_dao->find($id);
            $view->bind("pratics",$praticiens);
            
            $this->_dao = new VilleDAO();
            $city = $this->_dao->findFromPraticien($id);
            $view->bind("city", $city);
            
            $this->_dao = new TypePraticienDAO();
            $type = $this->_dao->findFromPraticien($id);
            $view->bind("type", $type);
            
            $this->_dao = new EtatDAO();
            $etat = $this->_dao->findFromProspect($id);
            $etats = $this->_dao->findAll();
            $view->bind("etat", $etat);
            $view->bind("etats", $etats);
    
            $view->display();
        }
        else
        {
            /**
             * Modifying Prospect in database
             */
            $idEtat = filter_input(INPUT_POST, 'etat', FILTER_SANITIZE_NUMBER_INT);
            $this->_dao = new ProspectDAO();
            $p = new Prospect($id,$idEtat,null,'','','');
            $prospects = $this->_dao->update($p);
            $basePath = $this->_router->getBasePath();
            header("Location: ".$basePath."/prospects/");
        }
    }
}